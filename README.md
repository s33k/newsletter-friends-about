# newsletter-friends

A news letter created and shared by and among friends.

## The Concept

This is a newsletter that can be edited and is shared among a limited number of people. To be specific I am making this for my friends and their friends. And how this gonna be work is, any of those people can come here and edit the Newsletter file before a particular day in a month. And once the day is reached I will finalize the information and make it into a newsletter and email it to each and every member disregarding you contributed or not. This way we are collecting news and information from many people you know or you don't and are shared with many people. I believe this will be a more productive and fruitful way to share news among peers. 

## For Who

Anyone interested. To start I invited my friends, and now you can invite yours. You will have to share their email and codeberg account information(if they are going to contribute) with us.

## Requirements

First you will need an email account to receive the montly newsletter. And then if you are planning to contribute you will need a codeberg account, if you don't have a codeberg account it takes only few seconds to make one. And if you are not comfortable creating one, you can email me(or your friend) to post your content for you. And the credits will be given to you. 

## Topics

There is no particular topic or a niche. You are free to write about literally anything. But I assume there will be many tech related content, because most people I invited are techies. But I hope there will be more. So yeah if you are good with poems or those artistic and creative things, you can share those too. And if you are educated about other subjects like sociology, education or anything like that. You are encouraged to write about those too.

## How to contribute?

<ol>
<li>First you need to have a codeberg account. </li>
<li>Send me your codeberg username so I can invite you (the repository is private)</li>
<li>Then you need to visit this repository https://codeberg.org/valhalla/newsletter-friends. </li>
<li>In here you will see all the files in the repository, and to edit or write you need to selct and click the file. Most probably it will look something like this "2021 Newsletter April.md".</li>
<li>Then the file will open, and you can see the content. So to edit or write you need to click the 'pencil' icon. </li>
<li>That's it, now you will be taken to a text editor where you can edit and write</li>
<li>After you are done writing (you can preview by clicking Preview) you can scroll down and commit your change. You can write if what changes you made there too (only if you want). </li>
<li>Then you can commit directly or make a new branch and start a pull request. You are allowed to do how you like, if you don't know if what that is don't think about it, just click commit changes.</li>
</ol>

## Things to keep in mind

Do not edit, replace or delete what others write. <br>
Start with a heading(level 2 will look good) and leave your name, nickname or pen name at the end.
Example:

### Fediverse

The Fediverse is a network of federated servers, that are used to publish social networks and other websites. Most of those servers are independently hosted, and can communicate among each server. In the fediverse we normally call these 'servers' as 'instances'. What is more important significant in Fediverse is, here the power of social media belongs to the people. Unlike birdsite(it's twitter we call it birdsite in the fediverse) and other social media like that. Here if one instance decided to you throw you away, don't worry you can just move to another instance. Same if one server went down for some reason, you will not have to worry because the rest will stand strong. And the fact that the whole fediverse is built using free and opensource software strengthens the freedom of the users.<br>
-SEEK

(then the next person should continue the document like this)
### Basic Intro to The AES-256 Cipher

AES stands for “Advanced Encryption Standard” and is a specification that has selected the Rijndael cipher as its symmetric key ciphering algorithm. AES encrypts a message with a private key and no one except the key holder can decrypt the message. This is useful for many reasons, but a good example is a laptop that encrypts the hard disk contents while at rest. <br>
-(Next person nick name)
<hr>

XMPP : newsletter-friends@muc.linuxcult.net <br>
     password : newsletter!<br>
*Enjoy!*